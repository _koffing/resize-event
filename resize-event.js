'use strict';


function resizeEvent(startcb, endcb){
    var resizing = false;
    var resizeTime;
    var resizeArgs = arguments.length;
    window.addEventListener("resize", function(){
        resizeTime = new Date().getTime();
        if(!resizing){
            resizing = true;
            var resizeCheck = setInterval(function(){
                var resizeCheckTimer = new Date().getTime();
                if((resizeTime + 50) < resizeCheckTimer){
                    resizing = false;
                    clearInterval(resizeCheck);
                    if(resizeArgs == 1){
                        return startcb();
                    } else if (resizeArgs == 2) {
                        return endcb();
                    }
                }
            }, 100);
            if(resizeArgs == 2){
                return startcb();
            }
        }
    });
}


# Resize Event #

resizeEvent is a javascript utility that allows a developer to fire off one main event at the start of the window resize event and a separate event at the end of window resize.

### Usage ###


* To run the event, call resizeEvent(callback1, callback2);
* callback1(optional) is the function that will be run when the resize event starts.
* Callback2(required) is the function that will be run after the resize event has finished.

### How do I get set up? ###

* Just include resize-event.js into your project by script tag, concatenation or past it in to your js file.

### example ###

resizeEvent(function(){  
    console.log("resize start");  
}, function(){  
    console.log("resize end");  
});